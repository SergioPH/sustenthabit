<!DOCTYPE html>
<html>
	<head>

<!-- Basic -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>SustentHabit</title>

<meta name="keywords" content="HTML5 Template" />
<meta name="description" content="Porto - Responsive HTML5 Template">
<meta name="author" content="okler.net">

<!-- Favicon -->
<link rel="shortcut icon" href="public/img/logo_header.png" type="image/x-icon" />
<link rel="apple-touch-icon" href="public/img/logo_header.png">

<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

<!-- Web Fonts  -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Poppins:400,700&display=swap" rel="stylesheet">
<!-- Vendor CSS -->
<link rel="stylesheet" href="public/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="public/vendor/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="public/vendor/animate/animate.min.css">
<link rel="stylesheet" href="public/vendor/simple-line-icons/css/simple-line-icons.min.css">
<link rel="stylesheet" href="public/vendor/owl.carousel/assets/owl.carousel.min.css">
<link rel="stylesheet" href="public/vendor/owl.carousel/assets/owl.theme.default.min.css">
<link rel="stylesheet" href="public/vendor/magnific-popup/magnific-popup.min.css">

<!-- Theme CSS -->
<link rel="stylesheet" href="public/css/theme.css">
<link rel="stylesheet" href="public/css/theme-elements.css">
<link rel="stylesheet" href="public/css/theme-blog.css">
<link rel="stylesheet" href="public/css/theme-shop.css">
<!-- Skin CSS -->
<link rel="stylesheet" href="public/css/skins/default.css">

<!-- Theme Custom CSS -->
<link rel="stylesheet" href="public/css/custom.css">

<!-- Head Libs -->
<script src="public/vendor/modernizr/modernizr.min.js"></script>

	</head>
	<body>
		<div class="body" style="background-color: #202123">

			@include('layouts.header')

				<div role="main" class="main">

					@yield('content')

				</div>
		</div>
		<!-- Vendor -->
		<script src="public/vendor/jquery/jquery.min.js"></script>
		<script src="public/vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="public/vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="public/vendor/jquery.cookie/jquery.cookie.min.js"></script>
		<script src="public/vendor/popper/umd/popper.min.js"></script>
		<script src="public/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="public/vendor/common/common.min.js"></script>
		<script src="public/vendor/jquery.validation/jquery.validate.min.js"></script>
		<script src="public/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
		<script src="public/vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="public/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="public/vendor/isotope/jquery.isotope.min.js"></script>
		<script src="public/vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="public/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="public/vendor/vide/jquery.vide.min.js"></script>
		<script src="public/vendor/vivus/vivus.min.js"></script>

		<!-- Theme Base, Components and Settings -->
		<script src="public/js/theme.js"></script>

		<!-- Theme Custom -->
		<script src="public/js/custom.js"></script>

		<!-- Theme Initialization Files -->
		<script src="public/js/theme.init.js"></script>
	</body>
</html>
