<header id="header" class="header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
				<div class="header-body border-top-0">
				<div class="header-container container" style="border-bottom: 1px solid #ffffff">
						<div class="header-row">
							<div class="header-column">
								<div class="header-row">
									<div class="header-logo">
										<a href="{{url('/')}}">
											<img alt="Porto" width="100" height="100" data-sticky-width="45" data-sticky-height="45" src="public/img/logo_header.png">
										</a>
									</div>
								</div>
							</div>
							<div class="header-column justify-content-end">
								<div class="header-row">
									<div class="header-nav header-nav-links header-nav-dropdowns-dark">
										<div class="header-nav-main header-nav-main-dropdown-no-borders header-nav-main-effect-2 header-nav-main-sub-effect-1">
											<nav class="collapse">
												<ul class="nav nav-pills" id="mainNav">
													<li class="dropdown">
														<a class="none dropdown-item dropdown-toggle" href="{{url('/proyectos')}}">
															WORK
														</a>
													</li>

													<li id="xs-none" class="dropdown">
														<a class="dropdown-item">
															/
														</a>
													</li>

													<li class="dropdown">
														<a class="pr-0 dropdown-item dropdown-toggle" >OFFICE<img class="pl-2" src="public/img/down.svg" width="30px" height="30px">
														</a>
														<ul class="dropdown-menu">
															<li>
																<a class="dropdown-item" href="{{url('arquitectura')}}">
																	ARQUITECTURA
																</a>
															</li>
															<li>
																<a class="dropdown-item" href="{{url('construccion')}}">
																	CONSTRUCCIÓN
																</a>
															</li>
														</ul>
													</li>

													<li id="xs-none" class="dropdown">
														<a class="dropdown-item">
															/
														</a>
													</li>
													<li class="dropdown">
														<a class="none dropdown-item dropdown-toggle" href="{{url('contactos')}}">
															CONTACT
														</a>

													</li>
												</ul>
											</nav>
										</div>
										<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
											<i class="fas fa-bars"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
