@extends('layouts.app')

@section('content')


<section style="padding-bottom: 2%">
	<div>
    	<video width="100%" poster="images/videos/explore.jpg" preload="auto" loop autoplay muted>
        	<source src='public/img/VIDEO_SUST_PEQUENO.mov' type='video/mp4' />
    	</video>
	</div>
</section>
<div class="xs-botones-float">
<a target="_blank" class="whts" style="display:none;" href="https://wa.me/529993021404?text=Me%20gustaría%20solicitar%20una%20cotización">
<img class="imageniconos" src="public/img/icono_whats.png">
</a>
<a href="tel:9993021404" class="phone" style="display:none;">
<img class="imageniconos" src="public/img/icono_llamada.png">
</a>
</div>
<section >
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center py-5">
					<img class="imagenlogohome" src="public/img/logo-palabra.png">
				</div>
				<div class="col-lg-12">
					<p class="texthome">
						Es arquitectura sustentable interesada en frenar el cambio climático, a través de la creación del diseño y
						construcción sustentables, que contribuyan a la conservación del medio ambiente generando armonía, confort,
						emotividad y experiencias entre el usuario y el entorno, desarollando estilos y tendencias vanguardistas
					</p>
				</div>

			</div>
		</div>
</section>
<footer id="footer">
	<div class="xs-heigth container-fluid px-4" style="height: 100px;">
		<div class="row" style="height: 100%;position: relative;">
			<div class="col-sm-4 width-40">
			<div class="xs-p0 row pl-5 d-flex align-items-end" style="height: 50%">
				<div class="pr-2">
					<a href="" target="_blank" >
						<img src="public/img/logo_footer1.png" width="40px">
					</a>
				</div>
				<div class="pr-2">
					<a href="" target="_blank" >
						<img src="public/img/logo_footer2.png" width="40px">
					</a>
				</div>
			</div>
				<div class="xs-p0 xs-no row pl-5 mt-3 d-flex align-items-end" style="height: 20%">
					<h5 style="text-transform: none;">Mapa de sitio&nbsp;&nbsp;>&nbsp;&nbsp;
					<a style="color:#fff" href="{{url('/')}}">sustenthabit</a></h5>
				</div>
			</div>
			<div class="width-60 col-sm-4 d-flex align-items-center justify-content-center">
				<div class="pr-4 pl-4">
					<a href="https://www.facebook.com/Sustenthabit/" target="_blank" >
						<img src="public/img/icono_redes1.png" width="30px">
					</a>
				</div>
				<div class="pr-4">
					<a href="https://twitter.com/sustenthabit" target="_blank" >
						<img src="public/img/icono_redes2.png" width="30px">
					</a>
				</div>
				<div class="pr-4">
					<a href="https://www.instagram.com/sustenthabit/" target="_blank" >
						<img src="public/img/icono_redes3.png" width="30px">
					</a>
				</div>
				<div class="">
					<a href="https://www.youtube.com/user/SustentHabit" target="_blank" >
						<img src="public/img/icono_redes4.png" width="30px">
					</a>
				</div>
			</div>

			<div class="col-sm-4">
			</div>
				<span style="color:#fff;position: absolute;right:0;bottom:0;">2020 Desarrollado por<a href="https://animatiomx.com/" style="color:#f3cc23;"> ANIMATIOMX</a><br>Contamos con once años de experiencia.</span>
		</div>
	</div>
</footer>
@endsection