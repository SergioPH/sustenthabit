	@extends('layouts.app')

	@section('content')
		<style>
@media (max-width: 991px)
{
.paddS {
    padding-left: 10px!important;
    padding-right: 10px!important;
}
}
</style>
<link href="https://fonts.googleapis.com/css?family=Poppins:300,600&display=swap" rel="stylesheet">
		<div class="container py-2">
		<div class="container text-center py-5">
			<br><br>
			<h1 class="txttitulo">
			CONSTRUCCIÓN SUSTENTABLE</h1>
		</div>

	</div>

	<div class="container py-3">
		<label class="textOffice" style="padding-bottom: 38px!important;">Sistema de edificación de tal modo que minimice el impacto
		ambiental de los edificios sobre el medio ambiente y sus habitantes. Si ya cuentas con los recursos necesarios para construir tu casa. SUSTENTHABIT te ayuda a desarrollar el proyecto arquitectónico, inicia la construcción de tu casa con un 30% de anticipo y el resto lo vas pagando conforme avanza la obra.</label>
		<h2 class="text-center" style="color:white;">TE ENTREGAMOS TU CASA CON ESTAS TECNOLOGÍAS</h2>
	<img src="public/img/Captura.png" class="mb-5" width="100%" height="100%" alt="">
	</div>

<div class="container-info" style="background-color: white">
<div style="text-align: center"><h1 class="proceso">PROCESO</h1></div>
		<div class="row mt-5">
			<div class="col paddS">
				<div class="row process my-5">
					<div class="process-step col-lg-4 mb-5 mb-lg-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">
						<div id="contenedor">
								<a href="javascript:void(0);" class="miniatura">
								<img src="public/img/proceso_diseno_arq/proceso_arq-01.png" alt="" style="width:100px;" />
								<span><img style="width:100px;" src="public/img/proceso_diseno_arq/proceso_arq-07.png" alt=""/></span>
								</a>
						</div>
						<div class="process-step-content">
							<h4 class="mb-1 text-5 font-weight-bold colorG">REVISIÓN DE PRESUPUESTO</h4>
							<p class="mb-0">Plática con el cliente donde <br> analizamos el presupuesto, <br> estilos y gustos.</p>
						</div>
					</div>
					<div class="process-step col-lg-4 mb-5 mb-lg-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">
						<div id="contenedor">
								<a href="javascript:void(0);" class="miniatura">
								<img src="public/img/proceso_diseno_arq/proceso_arq-02.png" alt="" style="width:100px;" />
								<span><img style="width:100px;" src="public/img/proceso_diseno_arq/proceso_arq-08.png" alt=""/></span>
								</a>
						</div>
						<div class="process-step-content">
							<h4 class="mb-1 text-5 font-weight-bold colorG">CONTRATO</h4>
							<p class="mb-0">Se firma un contrato para compromiso <br> de ambas partes. Se especifican <br> los pagos que el cliente <br> realizará durante el proceso.</p>
						</div>
					</div>
					<div class="process-step col-lg-4 mb-5 mb-lg-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="600">
						<div id="contenedor">
								<a href="javascript:void(0);" class="miniatura">
								<img src="public/img/proceso_diseno_arq/proceso_arq-03.png" alt="" style="width:100px;" />
								<span><img style="width:100px;" src="public/img/proceso_diseno_arq/proceso_arq-09.png" alt=""/></span>
								</a>
						</div>
						<div class="process-step-content">
							<h4 class="mb-1 text-5 font-weight-bold colorG">TRAMITES Y PERMISOS</h4>
							<p class="mb-0">El equipo realiza y consigue <br> los permisos necesarios <br> y de igual manera los tramites necesarios <br> sobre el terreno.</p>
						</div>
					</div>
				</div>
			</div>
	</div>
<div class="xs-botones-float">
<a target="_blank" class="whts" style="display:none;" href="https://wa.me/529993021404?text=Me%20gustaría%20solicitar%20una%20cotización">
<img class="imageniconos" src="public/img/icono_whats.png">
</a>
<a href="tel:9993021404" class="phone" style="display:none;">
<img class="imageniconos" src="public/img/icono_llamada.png">
</a>
</div>
	<div class="row mt-5">
			<div class="col paddS" style="margin-top: -80px;">
				<div class="row process my-5">
					<div class="process-step col-lg-4 mb-5 mb-lg-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">
						<div id="contenedor">
								<a href="javascript:void(0);" class="miniatura">
								<img src="public/img/proceso_diseno_arq/proceso_arq-05.png" alt="" style="width:100px;" />
								<span><img style="width:100px;" src="public/img/proceso_diseno_arq/proceso_arq-11.png" alt=""/></span>
								</a>
						</div>
						<div class="process-step-content">
							<h4 class="mb-1 text-5 font-weight-bold colorG">EJECUCIÓN DE OBRA</h4>
							<p class="mb-0">construccion del inmueble.</p>
						</div>
					</div>
					<div class="process-step col-lg-4 mb-5 mb-lg-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="600">
						<div id="contenedor">
								<a href="javascript:void(0);" class="miniatura">
								<img src="public/img/proceso_diseno_arq/proceso_arq-06.png" alt="" style="width:100px;" />
								<span><img style="width:100px;" src="public/img/proceso_diseno_arq/proceso_arq-12.png" alt=""/></span>
								</a>
						</div>
						<div class="process-step-content">
							<h4 class="mb-1 text-5 font-weight-bold colorG">ENTREGA Y CONTROL DE CALIDAD</h4>
						</div>
					</div>
				</div>
			</div>
	</div>
</div>

	<div class="container py-2">
		<div class="container text-center py-5">
			<br><br>
			<h3 class="txttitulo2">
			EMPIEZA HOY A CONSTRUIR LA BASE <br> DE TU HOGAR O NEGOCIO</h3><br>
			<button data-toggle="modal" data-target=".modal1" style="color:#202123;font-weight:900;font-size:20px;" class="btn btn-primary">
			VER COSTOS</button>
		</div>
	</div>

	<div class="container-info" style="background-color: white">

	<div class="row xs-no">
		<div class="col-lg-2"></div>
		<div class="col-lg-4">
				<img class="imagenmuchacho" src="public/img/muchacho.png">
		</div>
		<div class="col-lg-4">


			<div class="row">

				<div class="col-lg-12">
					<div style="text-align: center;">
					<label class="h1interes">¿ESTAS INTERESADO?</label>
					</div>
					</div>

				<div class="col-lg-6">
					<a target="_blank" href="https://wa.me/529993021404?text=Me%20gustaría%20solicitar%20una%20cotización">
					<img class="imageniconos" src="public/img/icono_whats.png">
					</a>
					<label class="textoicono" style="color: #00bf22">WhatsApp</label>
				</div>

				<div class="col-lg-6">
					<a href="tel:9993021404">
						<img class="imageniconos" src="public/img/icono_llamada.png">
					</a>
					<label class="textoicono" style="color: #333333">Llamada</label>
				</div>

			</div>
		</div>
		<div class="col-lg-2"></div>
	</div>
</div>
<footer id="footer">
	<div class="xs-heigth container-fluid px-4" style="height: 100px;">
		<div class="row" style="height: 100%;position: relative;">
			<div class="col-sm-4 width-40">
			<div class="xs-p0 row pl-5 d-flex align-items-end" style="height: 50%">
				<div class="pr-2">
					<a href="" target="_blank" >
						<img src="public/img/logo_footer1.png" width="40px">
					</a>
				</div>
				<div class="pr-2">
					<a href="" target="_blank" >
						<img src="public/img/logo_footer2.png" width="40px">
					</a>
				</div>
			</div>
				<div class="xs-p0 xs-no row pl-5 mt-3 d-flex align-items-end" style="height: 20%">
					<h5 style="text-transform: none;">Mapa de sitio&nbsp;&nbsp;>&nbsp;&nbsp;
					<a href="{{url('/')}}" style="color:#fff">sustenthabit</a>&nbsp;&nbsp;>&nbsp;&nbsp;<a href="construccion" style="color:#fff">construcción</a></h5>
				</div>
			</div>
			<div class="width-60 col-sm-4 d-flex align-items-center justify-content-center">
				<div class="pr-4 pl-4">
					<a href="https://www.facebook.com/Sustenthabit/" target="_blank" >
						<img src="public/img/icono_redes1.png" width="30px">
					</a>
				</div>
				<div class="pr-4">
					<a href="https://twitter.com/sustenthabit" target="_blank" >
						<img src="public/img/icono_redes2.png" width="30px">
					</a>
				</div>
				<div class="pr-4">
					<a href="https://www.instagram.com/sustenthabit/" target="_blank" >
						<img src="public/img/icono_redes3.png" width="30px">
					</a>
				</div>
				<div class="">
					<a href="https://www.youtube.com/user/SustentHabit" target="_blank" >
						<img src="public/img/icono_redes4.png" width="30px">
					</a>
				</div>
			</div>

			<div class="col-sm-4">
			</div>
				<span style="color:#fff;position: absolute;right:0;bottom:0;">2020 Desarrollado por<a href="https://animatiomx.com/" style="color:#f3cc23;"> ANIMATIOMX</a><br>Contamos con once años de experiencia.</span>
		</div>
	</div>
</footer>
<div class="modal fade bd-example-modal-lg modal1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <div class="modal-content" style="background-color:rgba(0, 0, 0, 0)!important;border:none;">
    	<div class="modal-header" style="border-bottom: 0">
	        <button style="background:#ffffff;border-radius:50%;padding-top:3px;padding-left:12px;height: 45px;width: 45px;opacity:1;margin-bottom: 5px;" type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span style="font-size: 40px;" aria-hidden="true">&times;</span>
	        </button>
      	</div>
		<div class="col-lg-12">
		<div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light owl-loaded owl-drag owl-carousel-init" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}" style="height: auto;">
		<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-550px, 0px, 0px); transition: all 0s ease 0s; width: 1650px;"><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
				<div class="img-thumbnail border-0 p-0 d-block">
					<img class="img-fluid border-radius-0" src="public/img/cons_precio1.jpg" alt="">
				</div>
			</div></div><div class="owl-item active" style="width: 540px; margin-right: 10px;"><div>
				<div class="img-thumbnail border-0 p-0 d-block">
					<img class="img-fluid border-radius-0" src="public/img/cons_precio2.jpg" alt="">
				</div>
			</div></div><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
				<div class="img-thumbnail border-0 p-0 d-block">
					<img class="img-fluid border-radius-0" src="public/img/cons_precio3.jpg" alt="">
				</div>
			</div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev"></button><button type="button" role="presentation" class="owl-next"></button></div><div class="owl-dots disabled"></div></div>
		</div>

    </div>
  </div>
</div>

	@endsection