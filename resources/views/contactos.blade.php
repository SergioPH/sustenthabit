	@extends('layouts.app')

	@section('content')
<style>
@media (max-width: 991px)
{
.pad{
   padding-left: 30px;
    padding-right: 30px;
}
}
</style>
	<div class="container py-2">
		<div class="container text-center py-5">
			<br><br>
			<h1 class="txttitulo">
			CONTACTO</h1><br>
			<h1 class="txttitulo1">
			VISÍTANOS, QUEREMOS VERTE</h1>
		</div>

	</div>

	<div class="container py-2 mt-5">

			<div class="row">
				<div class="col-lg-6">

					<div class="espacioinfocontacto">
					    <img class="iconocontacto" src="public/img/contacto_ubicacion.png">
		                <label class="textcontacto infocontacto">Sucursal Merida Yucatan
						</label>
					</div>

					<div class="espacioinfocontacto">
					    <img class="iconocontacto" src="public/img/contacto_llamada.png">
		                <label class="textcontacto infocontacto">
		                	9993021404
						</label>
					</div>

					<div class="">
					    <img class="iconocontacto" src="public/img/contacto_correo.png">
		                <label class="textcontacto infocontacto">
		                	hola@sustenthabit.com
						</label>
					</div>

				</div>

				<div class="col-lg-6" style="margin-top: -90px">
                    <iframe src="https://www.google.com/maps/d/u/0/embed?mid=1mFrtXdQUKMw7qQ9K_apyNFKRraFXv36P" width="500" height="450"></iframe>
                </div>

			</div>

	</div>
<div class="xs-botones-float">
<a target="_blank" class="whts" style="display:none;" href="https://wa.me/529993021404?text=Me%20gustaría%20solicitar%20una%20cotización">
<img class="imageniconos" src="public/img/icono_whats.png">
</a>
<a href="tel:9993021404" class="phone" style="display:none;">
<img class="imageniconos" src="public/img/icono_llamada.png">
</a>
</div>
	<div class="container py-2">
		<div class="container text-center" style="">
			<h1 class="txttitulo1">
			¿TIENES ALGUNA DUDA?</h1><br>
			<h1 class="txttitulo1">
			ESCRÍBENOS</h1>
		</div>

	</div>


						<div class="row pad">
							<div class="col-lg-2"></div>
							<div class="col-lg-8">

								<form id="contactForm" class="contact-form" action="php/contact-form.php" method="POST">
									<div class="contact-form-success alert alert-success d-none mt-4" id="contactSuccess">
										<strong>Success!</strong> Your message has been sent to us.
									</div>

									<div class="contact-form-error alert alert-danger d-none mt-4" id="contactError">
										<strong>Error!</strong> There was an error sending your message.
										<span class="mail-error-message text-1 d-block" id="mailErrorMessage"></span>
									</div>

									<div class="form-row">
										<div class="form-group col-lg-6">
											<label class="textformulario">Nombre</label>
											<input type="text" value="" data-msg-required="El nombre es necesario" maxlength="100" class="form-control" name="name" id="name" required>
										</div>
										<div class="form-group col-lg-6">
											<label class="textformulario">E-mail</label>
											<input type="email" value="" data-msg-required="El Correo electronico es necesario" data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-lg-6">
											<label class="textformulario">Asunto</label>
											<input type="text" value="" data-msg-required="Ingrese el asunto" maxlength="100" class="form-control" name="subject" id="subject" required>
										</div>
							            <div class="form-group col-lg-6">
											<label class="textformulario">Teléfono</label>
											<input type="text" value="" data-msg-required="El numero telefonico es necesario" maxlength="100" class="form-control" name="subject" id="subject" required>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col">
											<label class="textformulario">Mensaje</label>
											<textarea maxlength="5000" data-msg-required="Por favor ingrese su mensaje" rows="8" class="form-control" name="message" id="message" required></textarea>
										</div>
									</div>
									<div class="form-row text-center">
										<div class="form-group col">
											<button class="botonenviar" type="submit" data-loading-text="Loading...">ENVIAR</button>
										</div>
									</div>
								</form>

							</div>

							<div class="col-lg-2"></div>

						</div>

<br><br><br><br>

<div class="container-info" style="background-color: white">

	<div class="row xs-no">
		<div class="col-lg-2"></div>
		<div class="col-lg-4">
				<img class="imagenmuchacho" src="public/img/muchacho.png">
		</div>
		<div class="col-lg-4">


			<div class="row">

				<div class="col-lg-12">
					<div style="text-align: center;">
					<label class="h1interes">¿ESTAS INTERESADO?</label>
					</div>
					</div>

				<div class="col-lg-6">
					<a target="_blank" href="https://wa.me/529993021404?text=Me%20gustaría%20solicitar%20una%20cotización">
					<img class="imageniconos" src="public/img/icono_whats.png">
					</a>
					<label class="textoicono" style="color: #00bf22">WhatsApp</label>
				</div>

				<div class="col-lg-6">
					<a href="tel:9993021404">
						<img class="imageniconos" src="public/img/icono_llamada.png">
					</a>
					<label class="textoicono" style="color: #333333">Llamada</label>
				</div>

			</div>
		</div>
		<div class="col-lg-2"></div>
	</div>

</div>

<footer id="footer">
	<div class="xs-heigth container-fluid px-4" style="height: 100px;">
		<div class="row" style="height: 100%;position: relative;">
			<div class="col-sm-4 width-40">
			<div class="xs-p0 row pl-5 d-flex align-items-end" style="height: 50%">
				<div class="pr-2">
					<a href="" target="_blank" >
						<img src="public/img/logo_footer1.png" width="40px">
					</a>
				</div>
				<div class="pr-2">
					<a href="" target="_blank" >
						<img src="public/img/logo_footer2.png" width="40px">
					</a>
				</div>
			</div>
				<div class="xs-p0 xs-no row pl-5 mt-3 d-flex align-items-end" style="height: 20%">
					<h5 style="text-transform: none;">Mapa de sitio&nbsp;&nbsp;>&nbsp;&nbsp;
					<a style="color:#fff" href="{{url('/')}}">sustenthabit</a>&nbsp;&nbsp;>&nbsp;&nbsp;<a href="contactos" style="color:#fff">contacto</a></h5>
				</div>
			</div>
			<div class="width-60 col-sm-4 d-flex align-items-center justify-content-center">
				<div class="pr-4 pl-4">
					<a href="https://www.facebook.com/Sustenthabit/" target="_blank" >
						<img src="public/img/icono_redes1.png" width="30px">
					</a>
				</div>
				<div class="pr-4">
					<a href="https://twitter.com/sustenthabit" target="_blank" >
						<img src="public/img/icono_redes2.png" width="30px">
					</a>
				</div>
				<div class="pr-4">
					<a href="https://www.instagram.com/sustenthabit/" target="_blank" >
						<img src="public/img/icono_redes3.png" width="30px">
					</a>
				</div>
				<div class="">
					<a href="https://www.youtube.com/user/SustentHabit" target="_blank" >
						<img src="public/img/icono_redes4.png" width="30px">
					</a>
				</div>
			</div>

			<div class="col-sm-4">
			</div>
				<span style="color:#fff;position: absolute;right:0;bottom:0;">2020 Desarrollado por<a href="https://animatiomx.com/" style="color:#f3cc23;"> ANIMATIOMX</a><br>Contamos con once años de experiencia.</span>
		</div>
	</div>
</footer>
	@endsection