@extends('layouts.app')

@section('content')

<div class="col-lg-12">
<div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light owl-loaded owl-drag owl-carousel-init" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}" style="height: auto;">
<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-550px, 0px, 0px); transition: all 0s ease 0s; width: 1650px;"><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/umch/1.jpg" alt="">
		</div>
	</div></div><div class="owl-item active" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/umch/2.jpg" alt="">
		</div>
	</div></div><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/umch/4.jpg" alt="">
		</div>
	</div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev"></button><button type="button" role="presentation" class="owl-next"></button></div><div class="owl-dots disabled"></div></div>
</div>
<div class="container d-flex justify-content-center">
	<div class="col-lg-3">
		<div class="toggle toggle-primary" data-plugin-toggle="">
			<section class="toggle active">
				<label style="border: 0;background:transparent;">Oficinas Administrativas Union Mexicana de Chiapas</label>
				<div class="toggle-content" style="display: block;">
					<p>Departamentos sustentables en San Andres Cholula. Diseño
						arquitectónico basado en bioclimática.</p>
				</div>
			</section>
		</div>
	</div>
</div>
@endsection