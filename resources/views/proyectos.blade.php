@extends('layouts.app')

@section('content')

				<div class="container py-4 mb-5">
					<div class="container text-center py-5">
						<h1 style="color:#ffff;font-weight:bolder;font-size:50px">
						PROYECTOS</h1>
					</div>

					<ul class="nav nav-pills sort-source sort-source-style-3 justify-content-center" data-sort-id="portfolio" data-option-key="filter" data-plugin-options="{'layoutMode': 'masonry', 'filter': '*'}">
						<li class="nav-item active" data-option-value="*"><a class="nav-link text-1 text-uppercase active" href="#">TODOS</a></li>
						<li class="nav-item" data-option-value=".websites"><a class="nav-link text-1 text-uppercase" href="#">CABAÑAS</a></li>
						<li class="nav-item" data-option-value=".logos"><a class="nav-link text-1 text-uppercase" href="#">CASAS</a></li>
						<li class="nav-item" data-option-value=".brands"><a class="nav-link text-1 text-uppercase" href="#">OFICINAS</a></li>
						<li class="nav-item" data-option-value=".medias"><a class="nav-link text-1 text-uppercase" href="#">OTROS</a></li>
					</ul>

					<div class="sort-destination-loader sort-destination-loader-showing mt-4 pt-2">
						<div class="row portfolio-list sort-destination" data-sort-id="portfolio">


							<div class="col-md-6 col-lg-4 isotope-item logos">
								<div class="portfolio-item hover-effect-3d">
									<a data-toggle="modal" data-target=".modal1">
										<span class="thumb-info thumb-info-centered-info thumb-info-no-borders border-radius-0">
											<span class="thumb-info-wrapper border-radius-0">
												<img src="public/img/casa-arbol/casa-arbol1.jpg" class="img-fluid border-radius-0" alt="">
												<span class="thumb-info-title">
													<span class="thumb-info-inner">CASA DEL ARBOL</span>
												</span>

											</span>
										</span>
									</a>
								</div>
							</div>

							<div class="col-md-6 col-lg-4 isotope-item medias">
								<div class="portfolio-item hover-effect-3d">
									<a data-toggle="modal" data-target=".modal2">
										<span class="thumb-info thumb-info-centered-info thumb-info-no-borders border-radius-0">
											<span class="thumb-info-wrapper border-radius-0">
												<span class="owl-carousel owl-theme dots-inside m-0" data-plugin-options="{'items': 1, 'margin': 20, 'animateOut': 'fadeOut', 'autoplay': true, 'autoplayTimeout': 3000}"><span><img src="public/img/proyecto-encinos/SH_RENDER FACHADA 2.jpg" class="img-fluid border-radius-0" alt=""></span><span><img src="public/img/proyecto-encinos/SH_RENDER ACCESO.jpg" class="img-fluid border-radius-0" alt=""></span></span>
												<span class="thumb-info-title">
													<span class="thumb-info-inner">PROYECTO ENCINOS</span>

												</span>
											</span>
										</span>
									</a>
								</div>
							</div>

							<div class="col-md-6 col-lg-4 isotope-item logos">
								<div class="portfolio-item hover-effect-3d">
									<a data-toggle="modal" data-target=".modal3">
										<span class="thumb-info thumb-info-centered-info thumb-info-no-borders border-radius-0">
											<span class="thumb-info-wrapper border-radius-0">
												<img src="public/img/casa-pajaritos/casa-pajaritos.jpg" class="img-fluid border-radius-0" alt="">
												<span class="thumb-info-title">
													<span class="thumb-info-inner">CASA PAJARITOS</span>
												</span>
											</span>
										</span>
									</a>
								</div>
							</div>

							<div class="col-md-6 col-lg-4 isotope-item logos">
								<div class="portfolio-item hover-effect-3d">
									<a data-toggle="modal" data-target=".modal4">
										<span class="thumb-info thumb-info-centered-info thumb-info-no-borders border-radius-0">
											<span class="thumb-info-wrapper border-radius-0">
												<img src="public/img/casa-ulivarri/RENDER_CASA ULIVARRI_01.jpg" class="img-fluid border-radius-0" alt="">
												<span class="thumb-info-title">
													<span class="thumb-info-inner">CASA ULIVARRI</span>
												</span>
											</span>
										</span>
									</a>
								</div>
							</div>

							<div class="col-md-6 col-lg-4 isotope-item medias">
								<div class="portfolio-item hover-effect-3d">
									<a data-toggle="modal" data-target=".modal5">
										<span class="thumb-info thumb-info-centered-info thumb-info-no-borders border-radius-0">
											<span class="thumb-info-wrapper border-radius-0">
												<img src="public/img/auditorio-iasd/auditorio2.jpeg" class="img-fluid border-radius-0" alt="">
												<span class="thumb-info-title">
													<span class="thumb-info-inner">AUDITORIO IASD-CANCUN QUINTANA ROO</span>
												</span>
											</span>
										</span>
									</a>
								</div>
							</div>

							<div class="col-md-6 col-lg-4 isotope-item logos">
								<div class="portfolio-item hover-effect-3d">
									<a data-toggle="modal" data-target=".modal6">
										<span class="thumb-info thumb-info-centered-info thumb-info-no-borders border-radius-0">
											<span class="thumb-info-wrapper border-radius-0">
												<img src="public/img/casa-yenni/EXTERIOR-NOCHE.jpg" class="img-fluid border-radius-0" alt="">
												<span class="thumb-info-title">
													<span class="thumb-info-inner">CASA YENNI</span>
												</span>
											</span>
										</span>
									</a>
								</div>
							</div>

							<div class="col-md-6 col-lg-4 isotope-item brands">
								<div class="portfolio-item hover-effect-3d">
									<a data-toggle="modal" data-target=".modal7">
										<span class="thumb-info thumb-info-centered-info thumb-info-no-borders border-radius-0">
											<span class="thumb-info-wrapper border-radius-0">
												<img src="public/img/umch/ums.jpg" class="img-fluid border-radius-0" alt="">
												<span class="thumb-info-title">
												<span class="thumb-info-inner">OFICINAS ADMINISTRATIVAS UNION MEXICANA DE CHIAPAS</span>
												</span>
											</span>
										</span>
									</a>
								</div>
							</div>

							<div class="col-md-6 col-lg-4 isotope-item medias">
								<div class="portfolio-item hover-effect-3d">
									<a data-toggle="modal" data-target=".modal8">
										<span class="thumb-info thumb-info-centered-info thumb-info-no-borders border-radius-0">
											<span class="thumb-info-wrapper border-radius-0">
												<img src="public/img/escuela-serdan/serdan-7.jpg" class="img-fluid border-radius-0" alt="">
												<span class="thumb-info-title">
													<span class="thumb-info-inner">ESCUELA CARMEN SERDAN</span>
												</span>
											</span>
										</span>
									</a>
								</div>
							</div>

							<div class="col-md-6 col-lg-4 isotope-item medias">
								<div class="portfolio-item hover-effect-3d">
									<a data-toggle="modal" data-target=".modal9">
										<span class="thumb-info thumb-info-centered-info thumb-info-no-borders border-radius-0">
											<span class="thumb-info-wrapper border-radius-0">
												<img src="public/img/frailes/1.png" class="img-fluid border-radius-0" alt="">
												<span class="thumb-info-title">
													<span class="thumb-info-inner">FRAILES - SAN MIGUEL DE ALLENDE</span>
												</span>
											</span>
										</span>
									</a>
								</div>
							</div>

							<div class="col-md-6 col-lg-4 isotope-item websites">
								<div class="portfolio-item hover-effect-3d">
									<a data-toggle="modal" data-target=".modal10">
										<span class="thumb-info thumb-info-centered-info thumb-info-no-borders border-radius-0">
											<span class="thumb-info-wrapper border-radius-0">
												<img src="public/img/xalostoc/xalostoc-3.jpg" class="img-fluid border-radius-0" alt="">
												<span class="thumb-info-title">
													<span class="thumb-info-inner">CABAÑAS XALOSTOC</span>
												</span>
											</span>
										</span>
									</a>
								</div>
							</div>

							<div class="col-md-6 col-lg-4 isotope-item medias">
								<div class="portfolio-item hover-effect-3d">
									<a data-toggle="modal" data-target=".modal11">
										<span class="thumb-info thumb-info-centered-info thumb-info-no-borders border-radius-0">
											<span class="thumb-info-wrapper border-radius-0">
												<img src="public/img/iasd-yucatan/RAIDER-FACHADA.jpg" class="img-fluid border-radius-0" alt="">
												<span class="thumb-info-title">
													<span class="thumb-info-inner">IGLESIA UNIVERSITARIA IASD-MERIDA YUCATAN</span>
												</span>
											</span>
										</span>
									</a>
								</div>
							</div>

							<div class="col-md-6 col-lg-4 isotope-item brands">
								<div class="portfolio-item hover-effect-3d">
									<a data-toggle="modal" data-target=".modal12">
										<span class="thumb-info thumb-info-centered-info thumb-info-no-borders border-radius-0">
											<span class="thumb-info-wrapper border-radius-0">
												<img src="public/img/iasd-zapata/1.jpg" class="img-fluid border-radius-0" alt="">
												<span class="thumb-info-title">
													<span class="thumb-info-inner">OFICINAS ADMINISTRATIVAS IASD-EMILIANO ZAPATA</span>
												</span>
											</span>
										</span>
									</a>
								</div>
							</div>

							<div class="col-md-6 col-lg-4 isotope-item websites">
								<div class="portfolio-item hover-effect-3d">
									<a data-toggle="modal" data-target=".modal13">
										<span class="thumb-info thumb-info-centered-info thumb-info-no-borders border-radius-0">
											<span class="thumb-info-wrapper border-radius-0">
												<img src="public/img/malinche/RENDER_DOMO_02.jpg" class="img-fluid border-radius-0" alt="">
												<span class="thumb-info-title">
													<span class="thumb-info-inner">CABAÑA - MALINCHE</span>
												</span>
											</span>
										</span>
									</a>
								</div>
							</div>

						</div>
					</div>
				</div>
				<h2 class="text-center" style="color:white"><a href="https://www.sustenthabit.com/">*Para ver más proyectos visita sustenthabit.com</a></h2>
<div class="xs-botones-float">
<a target="_blank" class="whts" style="display:none;" href="https://wa.me/529993021404?text=Me%20gustaría%20solicitar%20una%20cotización">
<img class="imageniconos" src="public/img/icono_whats.png">
</a>
<a href="tel:9993021404" class="phone" style="display:none;">
<img class="imageniconos" src="public/img/icono_llamada.png">
</a>
</div>

<footer id="footer">
	<div class="xs-heigth container-fluid px-4" style="height: 100px;">
		<div class="row" style="height: 100%;position: relative;">
			<div class="col-sm-4 width-40">
			<div class="xs-p0 row pl-5 d-flex align-items-end" style="height: 50%">
				<div class="pr-2">
					<a href="" target="_blank" >
						<img src="public/img/logo_footer1.png" width="40px">
					</a>
				</div>
				<div class="pr-2">
					<a href="" target="_blank" >
						<img src="public/img/logo_footer2.png" width="40px">
					</a>
				</div>
			</div>
				<div class="xs-p0 xs-no row pl-5 mt-3 d-flex align-items-end" style="height: 20%">
					<h5 style="text-transform: none;">Mapa de sitio&nbsp;&nbsp;>&nbsp;&nbsp;
					<a href="{{url('/')}}" style="color:#fff">sustenthabit</a>&nbsp;&nbsp;>&nbsp;&nbsp;<a href="proyectos" style="color:#fff">proyectos</a></h5>
				</div>
			</div>
			<div class="width-60 col-sm-4 d-flex align-items-center justify-content-center">
				<div class="pr-4 pl-4">
					<a href="https://www.facebook.com/Sustenthabit/" target="_blank" >
						<img src="public/img/icono_redes1.png" width="30px">
					</a>
				</div>
				<div class="pr-4">
					<a href="https://twitter.com/sustenthabit" target="_blank" >
						<img src="public/img/icono_redes2.png" width="30px">
					</a>
				</div>
				<div class="pr-4">
					<a href="https://www.instagram.com/sustenthabit/" target="_blank" >
						<img src="public/img/icono_redes3.png" width="30px">
					</a>
				</div>
				<div class="">
					<a href="https://www.youtube.com/user/SustentHabit" target="_blank" >
						<img src="public/img/icono_redes4.png" width="30px">
					</a>
				</div>
			</div>

			<div class="col-sm-4">
			</div>
				<span style="color:#fff;position: absolute;right:0;bottom:0;">2020 Desarrollado por<a href="https://animatiomx.com/" style="color:#f3cc23;"> ANIMATIOMX</a><br>Contamos con once años de experiencia.</span>
		</div>
	</div>
</footer>

<div class="modal fade bd-example-modal-lg modal1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <div class="modal-content" style="background-color:#202123;">
    	<div class="modal-header" style="border-bottom: 0">
		    <h1 class="modal-title ml-auto" id="exampleModalLabel" style="color: white;">
		    	CASA DEL ARBOL
		    </h1>
	        <button style="background:#ffffff;border-radius:50%;padding-top:3px;padding-left:12px;height: 45px;width: 45px;opacity:1;" type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span style="font-size: 40px;" aria-hidden="true">&times;</span>
	        </button>
      	</div>
	<div class="col-lg-12">
<div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light owl-loaded owl-drag owl-carousel-init" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}" style="height: auto;">
<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-550px, 0px, 0px); transition: all 0s ease 0s; width: 1650px;"><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/casa-arbol/casa-arbol1.jpg" alt="">
		</div>
	</div></div><div class="owl-item active" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/casa-arbol/casa-arbol2.jpg" alt="">
		</div>
	</div></div><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/casa-arbol/casa-arbol3.jpg" alt="">
		</div>
	</div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev"></button><button type="button" role="presentation" class="owl-next"></button></div><div class="owl-dots disabled"></div></div>
</div>
<div class="container d-flex justify-content-center">
	<div class="col-lg-3">
		<div class="toggle toggle-primary" data-plugin-toggle="">
			<section class="toggle active">
				<label style="border: 0;background:transparent;">CASA DEL ARBOL</label>
				<div class="toggle-content" style="display: block;">
					<p>Departamentos sustentables en San Andres Cholula. Diseño
						arquitectónico basado en bioclimática.</p>
				</div>
			</section>
		</div>
	</div>
</div>

    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg modal2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <div class="modal-content" style="background-color:#202123;">
    	<div class="modal-header" style="border-bottom: 0">
		    <h1 class="modal-title ml-auto" id="exampleModalLabel" style="color: white;">
		    	PROYECTO ENCINOS
		    </h1>
	        <button style="background:#ffffff;border-radius:50%;padding-top:3px;padding-left:12px;height: 45px;width: 45px;opacity:1;" type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span style="font-size: 40px;" aria-hidden="true">&times;</span>
	        </button>
      	</div>
<div class="col-lg-12">
<div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light owl-loaded owl-drag owl-carousel-init" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}" style="height: auto;">
<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-550px, 0px, 0px); transition: all 0s ease 0s; width: 1650px;"><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/proyecto-encinos/1.jpg" alt="">
		</div>
	</div></div><div class="owl-item active" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/proyecto-encinos/2.jpg" alt="">
		</div>
	</div></div><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/proyecto-encinos/3.jpg" alt="">
		</div>
	</div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev"></button><button type="button" role="presentation" class="owl-next"></button></div><div class="owl-dots disabled"></div></div>
</div>
<div class="container d-flex justify-content-center">
	<div class="col-lg-3">
		<div class="toggle toggle-primary" data-plugin-toggle="">
			<section class="toggle active">
				<label style="border: 0;background:transparent;">PROYECTO ENCINOS</label>
				<div class="toggle-content" style="display: block;">
					<p>Departamentos sustentables en San Andres Cholula. Diseño
						arquitectónico basado en bioclimática.</p>
				</div>
			</section>
		</div>
	</div>
</div>

    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg modal3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <div class="modal-content" style="background-color:#202123;">
    	<div class="modal-header" style="border-bottom: 0">
		    <h1 class="modal-title ml-auto" id="exampleModalLabel" style="color: white;">
		    	CASA PAJARITOS
		    </h1>
	        <button style="background:#ffffff;border-radius:50%;padding-top:3px;padding-left:12px;height: 45px;width: 45px;opacity:1;" type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span style="font-size: 40px;" aria-hidden="true">&times;</span>
	        </button>
      	</div>
		<div class="col-lg-12">
		<div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light owl-loaded owl-drag owl-carousel-init" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}" style="height: auto;">
		<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-550px, 0px, 0px); transition: all 0s ease 0s; width: 1650px;"><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
				<div class="img-thumbnail border-0 p-0 d-block">
					<img class="img-fluid border-radius-0" src="public/img/casa-pajaritos/casa-pajaritos1.jpg" alt="">
				</div>
			</div></div><div class="owl-item active" style="width: 540px; margin-right: 10px;"><div>
				<div class="img-thumbnail border-0 p-0 d-block">
					<img class="img-fluid border-radius-0" src="public/img/casa-pajaritos/casa-pajaritos2.jpg" alt="">
				</div>
			</div></div><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
				<div class="img-thumbnail border-0 p-0 d-block">
					<img class="img-fluid border-radius-0" src="public/img/casa-pajaritos/casa-pajaritos3.jpg" alt="">
				</div>
			</div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev"></button><button type="button" role="presentation" class="owl-next"></button></div><div class="owl-dots disabled"></div></div>
		</div>
		<div class="container d-flex justify-content-center">
			<div class="col-lg-3">
				<div class="toggle toggle-primary" data-plugin-toggle="">
					<section class="toggle active">
						<label style="border: 0;background:transparent;">CASA PAJARITOS</label>
						<div class="toggle-content" style="display: block;">
							<p>Departamentos sustentables en San Andres Cholula. Diseño
								arquitectónico basado en bioclimática.</p>
						</div>
					</section>
				</div>
			</div>
		</div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg modal4" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <div class="modal-content" style="background-color:#202123;">
    	<div class="modal-header" style="border-bottom: 0">
		    <h1 class="modal-title ml-auto" id="exampleModalLabel" style="color: white;">
		    	CASA ULIVARRI
		    </h1>
	        <button style="background:#ffffff;border-radius:50%;padding-top:3px;padding-left:12px;height: 45px;width: 45px;opacity:1;" type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span style="font-size: 40px;" aria-hidden="true">&times;</span>
	        </button>
      	</div>
<div class="col-lg-12">
<div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light owl-loaded owl-drag owl-carousel-init" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}" style="height: auto;">
<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-550px, 0px, 0px); transition: all 0s ease 0s; width: 1650px;"><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/casa-ulivarri/RENDER_CASA ULIVARRI_01.jpg" alt="">
		</div>
	</div></div><div class="owl-item active" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/casa-ulivarri/RENDER_CASA ULIVARRI_02.jpg" alt="">
		</div>
	</div></div><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/casa-ulivarri/RENDER_CASA ULIVARRI_03.jpg" alt="">
		</div>
	</div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev"></button><button type="button" role="presentation" class="owl-next"></button></div><div class="owl-dots disabled"></div></div>
</div>
<div class="container d-flex justify-content-center">
	<div class="col-lg-3">
		<div class="toggle toggle-primary" data-plugin-toggle="">
			<section class="toggle active">
				<label style="border: 0;background:transparent;">CASA ULIVARRI</label>
				<div class="toggle-content" style="display: block;">
					<p>Departamentos sustentables en San Andres Cholula. Diseño
						arquitectónico basado en bioclimática.</p>
				</div>
			</section>
		</div>
	</div>
</div>

    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg modal5" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <div class="modal-content" style="background-color:#202123;">
    	<div class="modal-header" style="border-bottom: 0">
		    <h1 class="modal-title ml-auto" id="exampleModalLabel" style="color: white;">
		    	Auditorio IASD-Cancun Quintana Roo
		    </h1>
	        <button style="background:#ffffff;border-radius:50%;padding-top:3px;padding-left:12px;height: 45px;width: 45px;opacity:1;" type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span style="font-size: 40px;" aria-hidden="true">&times;</span>
	        </button>
      	</div>
<div class="col-lg-12">
<div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light owl-loaded owl-drag owl-carousel-init" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}" style="height: auto;">
<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-550px, 0px, 0px); transition: all 0s ease 0s; width: 1650px;"><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/auditorio-iasd/auditorio1.jpeg" alt="">
		</div>
	</div></div><div class="owl-item active" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/auditorio-iasd/auditorio2.jpeg" alt="">
		</div>
	</div></div><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/auditorio-iasd/auditorio3.jpeg" alt="">
		</div>
	</div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev"></button><button type="button" role="presentation" class="owl-next"></button></div><div class="owl-dots disabled"></div></div>
</div>
<div class="container d-flex justify-content-center">
	<div class="col-lg-3">
		<div class="toggle toggle-primary" data-plugin-toggle="">
			<section class="toggle active">
				<label style="border: 0;background:transparent;">Auditorio IASD-Cancun Quintana Roo</label>
				<div class="toggle-content" style="display: block;">
					<p>Departamentos sustentables en San Andres Cholula. Diseño
						arquitectónico basado en bioclimática.</p>
				</div>
			</section>
		</div>
	</div>
</div>

    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg modal6" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <div class="modal-content" style="background-color:#202123;">
    	<div class="modal-header" style="border-bottom: 0">
		    <h1 class="modal-title ml-auto" id="exampleModalLabel" style="color: white;">
		    	CASA YENNI
		    </h1>
	        <button style="background:#ffffff;border-radius:50%;padding-top:3px;padding-left:12px;height: 45px;width: 45px;opacity:1;" type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span style="font-size: 40px;" aria-hidden="true">&times;</span>
	        </button>
      	</div>
<div class="col-lg-12">
<div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light owl-loaded owl-drag owl-carousel-init" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}" style="height: auto;">
<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-550px, 0px, 0px); transition: all 0s ease 0s; width: 1650px;"><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/casa-yenni/COCINA-FINAL2.jpg" alt="">
		</div>
	</div></div><div class="owl-item active" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/casa-yenni/habitacion-raw2.jpg" alt="">
		</div>
	</div></div><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/casa-yenni/RENDER-SALA.jpg" alt="">
		</div>
	</div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev"></button><button type="button" role="presentation" class="owl-next"></button></div><div class="owl-dots disabled"></div></div>
</div>
<div class="container d-flex justify-content-center">
	<div class="col-lg-3">
		<div class="toggle toggle-primary" data-plugin-toggle="">
			<section class="toggle active">
				<label style="border: 0;background:transparent;">CASA YENNI</label>
				<div class="toggle-content" style="display: block;">
					<p>Departamentos sustentables en San Andres Cholula. Diseño
						arquitectónico basado en bioclimática.</p>
				</div>
			</section>
		</div>
	</div>
</div>

    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg modal7" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <div class="modal-content" style="background-color:#202123;">
    	<div class="modal-header" style="border-bottom: 0">
		    <h1 class="modal-title ml-auto" id="exampleModalLabel" style="color: white;">
		    	OFICINAS ADMINISTRATIVAS UNION MEXICANA DE CHIAPAS
		    </h1>
	        <button style="background:#ffffff;border-radius:50%;padding-top:3px;padding-left:12px;height: 45px;width: 45px;opacity:1;" type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span style="font-size: 40px;" aria-hidden="true">&times;</span>
	        </button>
      	</div>

<div class="col-lg-12">
<div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light owl-loaded owl-drag owl-carousel-init" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}" style="height: auto;">
<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-550px, 0px, 0px); transition: all 0s ease 0s; width: 1650px;"><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/umch/1.jpg" alt="">
		</div>
	</div></div><div class="owl-item active" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/umch/2.jpg" alt="">
		</div>
	</div></div><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/umch/4.jpg" alt="">
		</div>
	</div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev"></button><button type="button" role="presentation" class="owl-next"></button></div><div class="owl-dots disabled"></div></div>
</div>
<div class="container d-flex justify-content-center">
	<div class="col-lg-3">
		<div class="toggle toggle-primary" data-plugin-toggle="">
			<section class="toggle active">
				<label style="border: 0;background:transparent;">OFICINAS ADMINISTRATIVAS UNION MEXICANA DE CHIAPAS</label>
				<div class="toggle-content" style="display: block;">
					<p>Departamentos sustentables en San Andres Cholula. Diseño
						arquitectónico basado en bioclimática.</p>
				</div>
			</section>
		</div>
	</div>
</div>

    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg modal8" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <div class="modal-content" style="background-color:#202123;">
    	<div class="modal-header" style="border-bottom: 0">
		    <h1 class="modal-title ml-auto" id="exampleModalLabel" style="color: white;">
		    	ESCUELA CARMEN SERDAN
		    </h1>
	        <button style="background:#ffffff;border-radius:50%;padding-top:3px;padding-left:12px;height: 45px;width: 45px;opacity:1;" type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span style="font-size: 40px;" aria-hidden="true">&times;</span>
	        </button>
      	</div>
<div class="col-lg-12">
<div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light owl-loaded owl-drag owl-carousel-init" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}" style="height: auto;">
<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-550px, 0px, 0px); transition: all 0s ease 0s; width: 1650px;"><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/escuela-serdan/serdan-7.jpg" alt="">
		</div>
	</div></div><div class="owl-item active" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/escuela-serdan/serdan-8.jpg" alt="">
		</div>
	</div></div><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/escuela-serdan/serdan-9.jpg" alt="">
		</div>
	</div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev"></button><button type="button" role="presentation" class="owl-next"></button></div><div class="owl-dots disabled"></div></div>
</div>
<div class="container d-flex justify-content-center">
	<div class="col-lg-3">
		<div class="toggle toggle-primary" data-plugin-toggle="">
			<section class="toggle active">
				<label style="border: 0;background:transparent;">ESCUELA CARMEN SERDAN</label>
				<div class="toggle-content" style="display: block;">
					<p>Departamentos sustentables en San Andres Cholula. Diseño
						arquitectónico basado en bioclimática.</p>
				</div>
			</section>
		</div>
	</div>
</div>

    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg modal9" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <div class="modal-content" style="background-color:#202123;">
    	<div class="modal-header" style="border-bottom: 0">
		    <h1 class="modal-title ml-auto" id="exampleModalLabel" style="color: white;">
		     FRAILES - SAN MIGUEL DE ALLENDE
		    </h1>
	        <button style="background:#ffffff;border-radius:50%;padding-top:3px;padding-left:12px;height: 45px;width: 45px;opacity:1;" type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span style="font-size: 40px;" aria-hidden="true">&times;</span>
	        </button>
      	</div>

<div class="col-lg-12">
<div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light owl-loaded owl-drag owl-carousel-init" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}" style="height: auto;">
<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-550px, 0px, 0px); transition: all 0s ease 0s; width: 1650px;"><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/frailes/1.png" alt="">
		</div>
	</div></div><div class="owl-item active" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/frailes/2.png" alt="">
		</div>
	</div></div><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/frailes/RENDER_EXTERIOR.jpg" alt="">
		</div>
	</div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev"></button><button type="button" role="presentation" class="owl-next"></button></div><div class="owl-dots disabled"></div></div>
</div>
<div class="container d-flex justify-content-center">
	<div class="col-lg-3">
		<div class="toggle toggle-primary" data-plugin-toggle="">
			<section class="toggle active">
				<label style="border: 0;background:transparent;">FRAILES - SAN MIGUEL DE ALLENDE</label>
				<div class="toggle-content" style="display: block;">
					<p>Departamentos sustentables en San Andres Cholula. Diseño
						arquitectónico basado en bioclimática.</p>
				</div>
			</section>
		</div>
	</div>
</div>

    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg modal10" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <div class="modal-content" style="background-color:#202123;">
    	<div class="modal-header" style="border-bottom: 0">
		    <h1 class="modal-title ml-auto" id="exampleModalLabel" style="color: white;">
		     CABAÑAS XALOSTOC
		    </h1>
	        <button style="background:#ffffff;border-radius:50%;padding-top:3px;padding-left:12px;height: 45px;width: 45px;opacity:1;" type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span style="font-size: 40px;" aria-hidden="true">&times;</span>
	        </button>
      	</div>

<div class="col-lg-12">
<div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light owl-loaded owl-drag owl-carousel-init" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}" style="height: auto;">
<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-550px, 0px, 0px); transition: all 0s ease 0s; width: 1650px;"><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/xalostoc/xalostoc-4.jpg" alt="">
		</div>
	</div></div><div class="owl-item active" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/xalostoc/xalostoc-6.jpg" alt="">
		</div>
	</div></div><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/xalostoc/xalostoc-9.jpg" alt="">
		</div>
	</div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev"></button><button type="button" role="presentation" class="owl-next"></button></div><div class="owl-dots disabled"></div></div>
</div>
<div class="container d-flex justify-content-center">
	<div class="col-lg-3">
		<div class="toggle toggle-primary" data-plugin-toggle="">
			<section class="toggle active">
				<label style="border: 0;background:transparent;">CABAÑAS XALOSTOC</label>
				<div class="toggle-content" style="display: block;">
					<p>Departamentos sustentables en San Andres Cholula. Diseño
						arquitectónico basado en bioclimática.</p>
				</div>
			</section>
		</div>
	</div>
</div>

    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg modal11" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <div class="modal-content" style="background-color:#202123;">
		<div class="modal-header" style="border-bottom: 0">
		    <h1 class="modal-title ml-auto" id="exampleModalLabel" style="color: white;">
		     IGLESIA UNIVERSITARIA IASD-MERIDA YUCATAN
		    </h1>
	        <button style="background:#ffffff;border-radius:50%;padding-top:3px;padding-left:12px;height: 45px;width: 45px;opacity:1;" type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span style="font-size: 40px;" aria-hidden="true">&times;</span>
	        </button>
      	</div>
<div class="col-lg-12">
<div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light owl-loaded owl-drag owl-carousel-init" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}" style="height: auto;">
<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-550px, 0px, 0px); transition: all 0s ease 0s; width: 1650px;"><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/iasd-yucatan/yucatan-1.jpeg" alt="">
		</div>
	</div></div><div class="owl-item active" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/iasd-yucatan/yucatan-3.jpeg" alt="">
		</div>
	</div></div><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/iasd-yucatan/yucatan-4.jpeg" alt="">
		</div>
	</div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev"></button><button type="button" role="presentation" class="owl-next"></button></div><div class="owl-dots disabled"></div></div>
</div>
<div class="container d-flex justify-content-center">
	<div class="col-lg-3">
		<div class="toggle toggle-primary" data-plugin-toggle="">
			<section class="toggle active">
				<label style="border: 0;background:transparent;">IGLESIA UNIVERSITARIA IASD-MERIDA YUCATAN</label>
				<div class="toggle-content" style="display: block;">
					<p>Departamentos sustentables en San Andres Cholula. Diseño
						arquitectónico basado en bioclimática.</p>
				</div>
			</section>
		</div>
	</div>
</div>

    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg modal12" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <div class="modal-content" style="background-color:#202123;">
		<div class="modal-header" style="border-bottom: 0">
		    <h1 class="modal-title ml-auto" id="exampleModalLabel" style="color: white;">
		     OFICINAS ADMINISTRATIVAS IASD-EMILIANO ZAPATA
		    </h1>
	        <button style="background:#ffffff;border-radius:50%;padding-top:3px;padding-left:12px;height: 45px;width: 45px;opacity:1;" type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span style="font-size: 40px;" aria-hidden="true">&times;</span>
	        </button>
      	</div>
<div class="col-lg-12">
<div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light owl-loaded owl-drag owl-carousel-init" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}" style="height: auto;">
<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-550px, 0px, 0px); transition: all 0s ease 0s; width: 1650px;"><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/iasd-zapata/3.jpg" alt="">
		</div>
	</div></div><div class="owl-item active" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/iasd-zapata/4.jpg" alt="">
		</div>
	</div></div><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/iasd-zapata/5.jpg" alt="">
		</div>
	</div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev"></button><button type="button" role="presentation" class="owl-next"></button></div><div class="owl-dots disabled"></div></div>
</div>
<div class="container d-flex justify-content-center">
	<div class="col-lg-3">
		<div class="toggle toggle-primary" data-plugin-toggle="">
			<section class="toggle active">
				<label style="border: 0;background:transparent;">OFICINAS ADMINISTRATIVAS IASD-EMILIANO ZAPATA</label>
				<div class="toggle-content" style="display: block;">
					<p>Departamentos sustentables en San Andres Cholula. Diseño
						arquitectónico basado en bioclimática.</p>
				</div>
			</section>
		</div>
	</div>
</div>

    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg modal13" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <div class="modal-content" style="background-color:#202123;">
    	<div class="modal-header" style="border-bottom: 0">
		    <h1 class="modal-title ml-auto" id="exampleModalLabel" style="color: white;">
		        CABAÑA - MALINCHE
		    </h1>
	        <button style="background:#ffffff;border-radius:50%;padding-top:3px;padding-left:12px;height: 45px;width: 45px;opacity:1;" type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span style="font-size: 40px;" aria-hidden="true">&times;</span>
	        </button>
      	</div>
<div class="col-lg-12">
<div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-light owl-loaded owl-drag owl-carousel-init" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}" style="height: auto;">
<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-550px, 0px, 0px); transition: all 0s ease 0s; width: 1650px;"><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/malinche/RENDER_DOMO_01.jpg" alt="">
		</div>
	</div></div><div class="owl-item active" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/malinche/RENDER_DOMO_02.jpg" alt="">
		</div>
	</div></div><div class="owl-item" style="width: 540px; margin-right: 10px;"><div>
		<div class="img-thumbnail border-0 p-0 d-block">
			<img class="img-fluid border-radius-0" src="public/img/malinche/RENDER_DOMO_01.jpg" alt="">
		</div>
	</div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev"></button><button type="button" role="presentation" class="owl-next"></button></div><div class="owl-dots disabled"></div></div>
</div>
<div class="container d-flex justify-content-center">
	<div class="col-lg-3">
		<div class="toggle toggle-primary" data-plugin-toggle="">
			<section class="toggle active">
				<label style="border: 0;background:transparent;">CABAÑA - MALINCHE</label>
				<div class="toggle-content" style="display: block;">
					<p>Departamentos sustentables en San Andres Cholula. Diseño
						arquitectónico basado en bioclimática.</p>
				</div>
			</section>
		</div>
	</div>
</div>

    </div>
  </div>
</div>
@endsection