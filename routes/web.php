<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/proyectos', function () {
    return view('proyectos');
});
Route::get('contactos', function () {
    return view('contactos');
});
Route::get('arquitectura', function () {
    return view('arquitectura');
});
Route::get('construccion', function () {
    return view('construccion');
});

/*
|--------------------------------------------------------------------------
|         RUTAS DE LOS PROYECTOS ESPECIFICOS
|--------------------------------------------------------------------------
*/

Route::get('casa-del-arbol', function () {
    return view('proyectos.casa-arbol');
});
Route::get('proyecto-encinos', function () {
    return view('proyectos.proyecto-encinos');
});
Route::get('casa-pajaritos', function () {
    return view('proyectos.casa-pajaritos');
});
Route::get('casa-ulivarri', function () {
    return view('proyectos.casa-ulivarri');
});
Route::get('auditorio-IASD-cancun-quintana-roo', function () {
    return view('proyectos.auditorio-iasd');
});
Route::get('casa-yenni', function () {
    return view('proyectos.casa-yeni');
});
Route::get('oficinas-administrativas-umch', function () {
    return view('proyectos.umch');
});
Route::get('escuela-carmen-serdan', function () {
    return view('proyectos.serdan');
});
Route::get('frailes-san-miguel-de-allende', function () {
    return view('proyectos.frailes');
});
Route::get('cabañas-xalostoc', function () {
    return view('proyectos.xalostoc');
});
Route::get('iglesia-universitaria-IASD-merida-yucatan', function () {
    return view('proyectos.iasd-yucatan');
});
Route::get('oficinas-administrativas-IASD-emiliano-zapata', function () {
    return view('proyectos.iasd-zapata');
});

Route::get('cabaña-malinche', function () {
    return view('proyectos.malinche');
});